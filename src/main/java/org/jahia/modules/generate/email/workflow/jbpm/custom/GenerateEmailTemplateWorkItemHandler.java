package org.jahia.modules.generate.email.workflow.jbpm.custom;

import org.apache.commons.collections.CollectionUtils;
import org.jahia.bin.Render;
import org.jahia.services.content.JCRCallback;
import org.jahia.services.content.JCRNodeWrapper;
import org.jahia.services.content.JCRPublicationService;
import org.jahia.services.content.JCRSessionWrapper;
import org.jahia.services.content.JCRTemplate;
import org.jahia.services.notification.HttpClientService;
import org.jahia.services.workflow.jbpm.custom.AbstractWorkItemHandler;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jcr.RepositoryException;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class GenerateEmailTemplateWorkItemHandler extends AbstractWorkItemHandler implements WorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenerateEmailTemplateWorkItemHandler.class);

    @Autowired
    private transient HttpClientService httpClientService;

    @Override
    public void executeWorkItem(final WorkItem workItem,
                                final WorkItemManager manager) {
        @SuppressWarnings("unchecked")
        final List<String> info = (List<String>) workItem.getParameter("nodeIds");
        final String workspace = (String) workItem.getParameter("workspace");
        try {
            JCRTemplate.getInstance().doExecuteWithSystemSessionInSameWorkspaceAndLocale(new JCRCallback<String>() {
                public String doInJCR(final JCRSessionWrapper session) throws RepositoryException {
                    if (CollectionUtils.isNotEmpty(info)) {
                        for (final String id : info) {
                            final JCRNodeWrapper node = session.getNodeByIdentifier(id);
                            if ("jnt:page".equals(node.getPrimaryNodeTypeName())) {
                                //TODO: Make the url more configureable.

                                final String url = "http://localhost:8080"
                                        + Render.getRenderServletPath()
                                        + "/live/"
                                        + node.getResolveSite().getDefaultLanguage() + node.getPath()
                                        + ".html?externalize=true";
                                final String out = httpClientService.executeGet(url);
                                LOGGER.info(out);
                                try {
                                    //TODO: For POC, place in temp folder using the node name as the file name.
                                    final File file = new File("../temp/" + node.getName() + ".html");
                                    final FileWriter fileWriter = new FileWriter(file);
                                    fileWriter.write(out);
                                    fileWriter.flush();
                                    fileWriter.close();
                                } catch (Exception e) {
                                    LOGGER.error(e.getMessage(), e);
                                }
                            }
                        }
                    }
                    return null;
                }
            });

            JCRPublicationService.getInstance().unlockForPublication(info, workspace,
                    "publication-process-" + workItem.getProcessInstanceId());
        } catch (RepositoryException e) {
            throw new RuntimeException(e);
        }
        manager.completeWorkItem(workItem.getId(), null);
    }

    @Override
    public void abortWorkItem(final WorkItem workItem,
                              final WorkItemManager manager) {
        manager.abortWorkItem(workItem.getId());
    }
}
